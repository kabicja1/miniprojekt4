

from flask import Flask, render_template, url_for, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin, login_user, LoginManager, login_required, logout_user, current_user
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import InputRequired, Length, ValidationError
from flask_bcrypt import Bcrypt
from routes import register_routes
from datetime import datetime
from sqlalchemy import REAL
import serial
from threading import Thread


#nebo import os

app = Flask(__name__)  # Creating an instance of the Flask class 
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db' #this connects app file to our database
#tady se dá použít db_file = os.path.join(os.getvwd(), 'My_Project', database.db)
app.config['SECRET_KEY'] = 'secretkey'
app.config['SESSION_TYPE'] = 'filesystem'  # nebo 'redis'
db = SQLAlchemy(app) #this create database instance
bcrypt = Bcrypt(app)

 
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'web_routes.login'
register_routes(app)





def generate_task_id():
    # Získání největšího ID z databáze Measurement
    max_id = db.session.query(db.func.max(Measurement.id)).scalar()

    if max_id is None:
        return 1
    else:
        
        return max_id + 1
    

def read_serial():
    ser = serial.Serial('COM4', 9600)  # Otevření sériového portu

    print("Čekám na data ze sériového portu...")
    try:
        while True:
            if ser.in_waiting > 0:
                line = ser.readline().decode('utf-8').strip()
                value = float(line)
                print(f"Přijata data: {line}, uložena do databáze.")
                return value
    except Exception as e:
        print("Chyba při čtení sériového portu:", str(e))
    finally:
        ser.close()  # Uzavření sériového portu po ukončení

class Measurement(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, nullable=False, default=lambda: datetime.now().replace(microsecond=0))
    temp = db.Column(REAL, nullable=False)



@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
 
class User(db.Model, UserMixin):#tady si vytvořím třídu pro uživatele
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), nullable=False, unique=True)
    password = db.Column(db.String(80), nullable=False)



class RegisterForm(FlaskForm):
    username = StringField(validators=[InputRequired(), Length(min=4, max=20)], render_kw={"placeholder": "Username"})

    password = PasswordField(validators=[InputRequired(), Length(min=8, max=20)], render_kw={"placeholder": "Password"})

    submit = SubmitField('Register')

    def validate_username(self, username):
        existing_user_username = User.query.filter_by(
            username=username.data).first()
        if existing_user_username:
            raise ValidationError(
                'That username already exists. Please choose a different one.')


class LoginForm(FlaskForm):
    username = StringField('username', validators=[InputRequired(), Length(min=4, max=20)])

    password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=20)])

    submit = SubmitField('Login')
# definuju pole a to pole dám templatu a templatu dám for cyklus a z toho to budu vypisovat
 

 
# Starting a web application at 0.0.0.0.0:5000 with debug mode enabled  
if __name__ == "__main__":  

    app.run(host='0.0.0.0', port=5000, debug=True, use_reloader=False)

